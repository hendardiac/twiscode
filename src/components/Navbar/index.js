import React from "react";
import { faGear } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

// assets
import logo from "../../logo.png";

// css
import "./Navbar.scss";

const Navbar = () => {
  return (
    <nav className="navbar">
      <div className="navbar__ruler navbar__ruler--top"></div>
      <div className="navbar__content">
        <img className="navbar__logo" src={logo} alt="twisocde logo" />
        <p className="navbar__language">
          Language :
          <select
            className="navbar__language__select"
            name="language"
            id="language"
          >
            <option value="0">English</option>
            <option value="1">Bahasa</option>
          </select>
          <FontAwesomeIcon icon={faGear}></FontAwesomeIcon>
        </p>
      </div>
      <div className="navbar__ruler navbar__ruler--bottom"></div>
    </nav>
  );
};

export default Navbar;
