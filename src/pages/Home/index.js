import React, { useEffect, useState } from "react";
import axios from "axios";
import {
  Autocomplete,
  Box,
  Button,
  Checkbox,
  FormControlLabel,
  FormGroup,
  Grid,
  InputAdornment,
  Modal,
  Stack,
  Switch,
  TextField,
} from "@mui/material";

import "./Home.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendar } from "@fortawesome/free-solid-svg-icons";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
  maxHeight: "80vh",
  overflow: "scroll",
};

const Home = () => {
  const [titleCheckbox, setTitleCheckbox] = useState({
    mrs: false,
    ms: false,
    mdm: false,
    mr: false,
    dr: false,
  });
  const [lastName, setLastName] = useState("");
  const [firstName, setFirstName] = useState("");
  const [selectedFlag, setSelectedFlag] = useState("");
  const [mobilePhoneNumber, setMobilePhoneNumber] = useState("");
  const [address, setAddress] = useState("");
  const [countryList, setCountryList] = useState([]);
  const [selectedCountry, setSelectedCountry] = useState("");
  const [selectedProvince, setSelectedProvince] = useState("");
  const [email, setEmail] = useState("");
  const [dob, setDob] = useState("");
  const [month, setMonth] = useState("");
  const [year, setYear] = useState("");
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [isMobileCallSelected, setIsMobileCallSelected] = useState(false);
  const [isEmailSelected, setIsEmailSelected] = useState(false);
  const [isMailingSelected, setIsMailingSelected] = useState(false);

  const fetchCountries = async () => {
    try {
      const response = await axios.get(`https://restcountries.com/v3.1/all`);

      const temp = response.data.map((country) => {
        return { flag_url: country.flags.png, label: country.name.common };
      });
      setCountryList(temp);
    } catch (error) {
      console.error(error);
    }
  };

  const handleOpenModal = () => {
    setIsOpenModal(true);
  };

  const handleCloseModal = () => {
    setIsOpenModal(false);
  };

  useEffect(() => {
    fetchCountries();
  }, []);

  return (
    <section className="home">
      <h3 className="home__notification">
        You don't have an account yet, please create a new account
      </h3>
      <div className="home__container">
        <h3>Create New Account</h3>
        <div className="home__input-container">
          <h4>Title</h4>
          <FormGroup row>
            <FormControlLabel
              control={
                <Checkbox
                  checked={titleCheckbox.mrs}
                  onChange={(e) =>
                    setTitleCheckbox((prevState) => ({
                      ...prevState,
                      mrs: !prevState.mrs,
                    }))
                  }
                  sx={{ color: "#ffcb01" }}
                />
              }
              label="Mrs"
            />
            <FormControlLabel
              control={
                <Checkbox
                  checked={titleCheckbox.ms}
                  onChange={(e) =>
                    setTitleCheckbox((prevState) => ({
                      ...prevState,
                      ms: !prevState.ms,
                    }))
                  }
                  sx={{ color: "#ffcb01" }}
                />
              }
              label="Ms"
            />
            <FormControlLabel
              control={
                <Checkbox
                  checked={titleCheckbox.mdm}
                  onChange={(e) =>
                    setTitleCheckbox((prevState) => ({
                      ...prevState,
                      mdm: !prevState.mdm,
                    }))
                  }
                  sx={{ color: "#ffcb01" }}
                />
              }
              label="Mdm"
            />
            <FormControlLabel
              control={
                <Checkbox
                  checked={titleCheckbox.mr}
                  onChange={(e) =>
                    setTitleCheckbox((prevState) => ({
                      ...prevState,
                      mr: !prevState.mr,
                    }))
                  }
                  sx={{ color: "#ffcb01" }}
                />
              }
              label="Mr"
            />
            <FormControlLabel
              control={
                <Checkbox
                  checked={titleCheckbox.dr}
                  onChange={(e) =>
                    setTitleCheckbox((prevState) => ({
                      ...prevState,
                      dr: !prevState.dr,
                    }))
                  }
                  sx={{ color: "#ffcb01" }}
                />
              }
              label="Dr"
            />
          </FormGroup>
          <Grid container spacing={2}>
            <Grid item xs={12} lg={6}>
              <h4 className="required">Last Name</h4>
              <TextField
                placeholder="Last Name"
                variant="outlined"
                fullWidth
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
              />
            </Grid>
            <Grid item xs={12} lg={6}>
              <h4 className="required">First Name</h4>
              <TextField
                placeholder="First Name"
                variant="outlined"
                fullWidth
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <h4 className="required">Mobile phone number</h4>
            </Grid>
            <Grid item xs={4} lg={1}>
              <Autocomplete
                onChange={(event, value) => {
                  setSelectedFlag(value);
                }}
                disableClearable
                options={countryList}
                autoHighlight
                getOptionLabel={(option) => option.label}
                renderOption={(props, option) => (
                  <Box
                    component="li"
                    sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
                    {...props}
                  >
                    <img
                      loading="lazy"
                      width="20"
                      src={option.flag_url}
                      srcSet={option.flag_url}
                      alt=""
                    />
                    {option.label}
                  </Box>
                )}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    variant="outlined"
                    fullWidth
                    InputProps={{
                      ...params.InputProps,
                      label: "",
                      startAdornment: (
                        <InputAdornment position="start">
                          {/* <img
                            style={{ height: "20px", width: "30px" }}
                            src={selectedFlag.flag_url}
                            alt=""
                          /> */}
                        </InputAdornment>
                      ),
                    }}
                  />
                )}
              />
            </Grid>
            <Grid item xs={8} lg={5}>
              <TextField
                placeholder="Mobile phone number"
                variant="outlined"
                fullWidth
                type="number"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">+62</InputAdornment>
                  ),
                }}
                value={mobilePhoneNumber}
                onChange={(e) => setMobilePhoneNumber(e.target.value)}
              />
            </Grid>
          </Grid>
        </div>
        <h3>Address</h3>
        <div className="home__input-container">
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <h4>Address</h4>
              <TextField
                placeholder="Address"
                variant="outlined"
                fullWidth
                value={address}
                onChange={(e) => setAddress(e.target.value)}
              />
            </Grid>
            <Grid item xs={12} lg={6}>
              <h4 className="required">Country/Location</h4>
              <Autocomplete
                onChange={(event, value) => {
                  setSelectedCountry(value);
                }}
                disablePortal
                options={countryList}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    placeholder="Select Country/Location"
                  />
                )}
              />
            </Grid>
            <Grid item xs={12} lg={6}>
              <h4 className="required">Province/Distric</h4>
              <Autocomplete
                onChange={(event, value) => {
                  setSelectedProvince(value);
                }}
                disablePortal
                options={countryList}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    placeholder="Select Province/Distric"
                  />
                )}
              />
            </Grid>
          </Grid>
        </div>
        <h3>Contacts</h3>
        <div className="home__input-container">
          <Grid container spacing={2}>
            <Grid item xs={12} lg={6}>
              <h4 className="required">Email Address</h4>
              <TextField
                placeholder="Email Address"
                variant="outlined"
                fullWidth
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </Grid>
            <Grid item xs={12} lg={6}>
              <Stack direction={{ xs: "column", lg: "row" }} spacing={2}>
                <div className="home__input-date">
                  <h4 className="required">Date of birth</h4>
                  <Autocomplete
                    onChange={(event, value) => {
                      setDob(value);
                    }}
                    disablePortal
                    options={[
                      "1",
                      "2",
                      "3",
                      "4",
                      "5",
                      "6",
                      "7",
                      "8",
                      "9",
                      "10",
                      "11",
                      "12",
                    ]}
                    renderInput={(params) => (
                      <TextField {...params} placeholder="DD" />
                    )}
                  />
                  <div className="home__input-date__alert">
                    Indulge in birthday treats just for you!
                  </div>
                </div>
                <div className="home__input-date">
                  <h4 className="required">Month</h4>
                  <TextField
                    placeholder="MM"
                    variant="outlined"
                    fullWidth
                    type="number"
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <FontAwesomeIcon icon={faCalendar} />
                        </InputAdornment>
                      ),
                    }}
                    value={month}
                    onChange={(e) => setMonth(e.target.value)}
                  />
                </div>
                <div className="home__input-date">
                  <h4>Year</h4>
                  <TextField
                    placeholder="YYYY"
                    variant="outlined"
                    fullWidth
                    type="number"
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <FontAwesomeIcon icon={faCalendar} />
                        </InputAdornment>
                      ),
                    }}
                    value={year}
                    onChange={(e) => setYear(e.target.value)}
                  />
                </div>
              </Stack>
            </Grid>
          </Grid>
        </div>
        <div className="home__privacy">
          <h3>Standard Privacy Note</h3>
          <p>
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ea amet
            accusamus, tempora corporis officiis exercitationem corrupti
            distinctio qui eveniet, optio obcaecati praesentium provident, in
            similique unde? Voluptate consectetur nihil vel quibusdam. In a
            repellendus commodi illum itaque dignissimos labore? Explicabo!
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Odio
            cupiditate recusandae animi est voluptate, labore debitis magnam
            vero quasi doloremque?
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Reprehenderit nemo dolorem sequi necessitatibus ad, officia
            similique reiciendis veniam cupiditate possimus ut libero, eligendi
            iste corrupti doloremque deleniti quo et commodi dicta itaque
            distinctio odio. Quaerat.
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero
            labore, sunt repudiandae nobis quod voluptate sit! Neque itaque
            veritatis quam.
          </p>
          <p>I want to receive information or communication from L'OCCITANE</p>

          <div className="home__privacy__switch__container">
            <div className="home__privacy__switch">
              <div>
                SMS & Mobile Call
                <Switch
                  onChange={(e) => setIsMobileCallSelected(e.target.checked)}
                  checked={isMobileCallSelected}
                />
              </div>
            </div>
            <div className="home__privacy__switch">
              <div>
                Emailing
                <Switch
                  onChange={(e) => setIsEmailSelected(e.target.checked)}
                  checked={isEmailSelected}
                />
              </div>
            </div>
            <div className="home__privacy__switch">
              <div>
                Mailing
                <Switch
                  onChange={(e) => setIsMailingSelected(e.target.checked)}
                  checked={isMailingSelected}
                />
              </div>
            </div>
          </div>
          <div className="home__privacy__alert">
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Omnis
            minus adipisci ad molestiae reiciendis quos necessitatibus est
            corrupti alias cumque illo eius, temporibus cum quasi optio et
            dolore culpa id.
          </div>
        </div>
        <div className="home__agreement">
          <FormGroup>
            <FormControlLabel
              sx={{ alignItems: "flex-start" }}
              control={<Checkbox sx={{ py: 0 }} />}
              label="Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero
                    labore, sunt repudiandae nobis quod voluptate sit! Neque itaque
                    veritatis quam."
            />
          </FormGroup>
        </div>
        <Box className="home__button__container">
          <Button
            className="home__button"
            variant="contained"
            onClick={handleOpenModal}
          >
            CREATE CUSTOMER
          </Button>
        </Box>
      </div>

      <Modal
        open={isOpenModal}
        onClose={handleCloseModal}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <h2>Title</h2>
          <h3>mrs: {titleCheckbox.mrs ? "checked" : "not checked"}</h3>
          <h3>ms: {titleCheckbox.ms ? "checked" : "not checked"}</h3>
          <h3>mdm: {titleCheckbox.mdm ? "checked" : "not checked"}</h3>
          <h3>mr: {titleCheckbox.mr ? "checked" : "not checked"}</h3>
          <h3>dr: {titleCheckbox.dr ? "checked" : "not checked"}</h3>
          <h2>
            Name : {lastName} {firstName}
          </h2>
          <h2>
            Mobile Phone Number : {selectedFlag.label} {mobilePhoneNumber}
          </h2>
          <h2>Address : {address}</h2>
          <h2>Country : {selectedCountry.label}</h2>
          <h2>Province : {selectedProvince.label}</h2>
          <h2>Email : {email}</h2>
          <h2>DOB : {dob}</h2>
          <h2>Month : {month}</h2>
          <h2>Year : {year}</h2>
          <h2>
            SMS & Mboile Call :
            {isMobileCallSelected ? "checked" : "not checked"}
          </h2>
          <h2>Email : {isEmailSelected ? "checked" : "not checked"}</h2>
          <h2>Mailing : {isMailingSelected ? "checked" : "not checked"}</h2>
        </Box>
      </Modal>
    </section>
  );
};

export default Home;
